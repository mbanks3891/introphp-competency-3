<?php
require_once 'comp3functions.php';
$conn = createConn();
writeHead("Comp 3 Required Part B", "Delete");


//start here on second run AKA after clicking the CONFIRM DELETE BUTTON
if (isset($_POST['confirmdelete'])) 
{// if SUBMIT BUTTON CLICKED 
$tid = $_POST['tid']; //gather contents from 'tid'
$query = "delete from Track where TrackId=$tid"; //specify query terms
mysqli_query($conn, $query) or die(mysqli_error($conn)); //form query

if (mysqli_affected_rows($conn)>0) 
{ //if something changed at database rows.......
header("Location: comp3partb2display.php?actionheadervardeletefromb5=deleted!!!&idheadervardeletefromb5=$tid"); 
//send the header to 3partb2display w/ actionHeaderVar=deleted!!! and idHeaderVar=$tid
//exit this page and end up on display page
exit();
}
//if NOTHING has changed
echo "<p class='error'>Unable to update record</p>";
} 


else //if BUTTON NOT CLICKED aka if URL DIRECTLY TO PAGE INSTEAD OF VIA LINK FROM display page
{
if (!isset($_GET['idLinkVarFromb2'])) 
{//if no id was passed in
echo "<p class='error'>No Data Provided!! Try starting from the <a href='comp3partb2display.php'>DISPLAY PAGE.</a>";
} 







//START HERE on first run upon being sent from display page......

//pull track id from querystring and use it to gather info for confirmation message display
$tid=$_GET['idLinkVarFromb2'];
$query = "Select * from Track where TrackId = $tid";
$result = mysqli_query($conn,$query);
if (!$result) {
die(mysqli_error($conn));
}

// check for results
if (mysqli_num_rows($result)> 0) {
// retrieve result row
$row = mysqli_fetch_assoc($result);
$trackid=$row['TrackId'];
$name=$row['Name'];
$unitprice=$row['UnitPrice'];
} 
else {
echo "<p class='error'>Unable to retrieve track $tid. <a href='comp3partb2display.php'>Return to display page.</a>";
}
}


?>

<!--DISPLAY confirmation message based on track we've selected on display page-->


<p>Are you sure you want to delete this <br><em>entire</em> record from the database?</p>
<p><?php 
echo 
"Track ID: <b>$trackid</b>
<br>Name: <b>$name</b>
<br>Price: <b>$unitprice</b>"; 
?></p>

<!--this form has one hidden field and one button, directs back to this page 3-5 delete... code starts at line 8-->
<form method="post" action="comp3partb5delete.php">
<p>
<input type="hidden" name="tid" value="<?php echo $tid; ?>">
<input type="submit" name="confirmdelete" value="Confirm Delete">
</p>
</form>
<p>Cancel &  <a href="comp3partb2display.php">Return to Display Page</a></p>


<?php writeFoot("3C"); ?>