<?php
require_once 'comp3functions.php';
$conn = createConn();
writeHead("Comp 3 Required Part B", "Update");



if(isset($_POST['confirmupdate']))
// if CONFIRM BUTTON CLICKED... (which will send you back to DISPLAY PAGE after updating datbase)
//(IF NOT, START ON LINE 160 instead of here)
{

$valid=true;

/////////////////////////////////////START GATHERING DATA FROM UPDATE FORM////////////////////////////////////////


/////////////////////FORM LINE 0 TRACK ID
/////////////
////hidden field on form
////no php validation needed
///ORIGINATES on this page at querystring sent from B2Display
///line 198
///$tid filled via GET... if running this page 1st time
///... and that val goes into the form at 'tid'

//if runnign this page 2nd time...i.e. you clicked UPDATE
//instead of being sent from display
//$tid is filled here at line 3, via form value

$tid=$_POST['tid'];//gather contents from fields that correspond to 'tid' hidden field on form below

//////////TEXT BOX///FORM LINE 1////////////////////// NAME aka Album Name //////////
//////////REQUIRED /////////////////// TEXT ///////// MAX 200 CHARS //////
$name = mysqli_real_escape_string($conn, trim($_POST['name']));
if (empty($name)) 
{
echo "<p class='error'>Please enter a name</p>";
$valid = false;
}
if (strlen($name>200))
{
echo "<p class='error'>(Name allows 200 characters max)</p>";
$valid=false;
}



//////////DROP DOWN SELECTION LIST///FORM LINE 2////////// ALBUM ID #////////
//////////REQUIRED ///////////////// DROP DOWN SELECTION /////////////////
$albumid = mysqli_real_escape_string($conn, trim($_POST['albumid']));
if (empty($albumid)) 
{
echo "<p class='error'>Please enter an album id</p>";
$valid = false;
}





//////////TEXT BOX///FORM LINE 3//////////////////// MEDIA TYPE ID /////
//////////PRESET ////////////////// TEXT /////
///SET to 2
$mediatypeid = mysqli_real_escape_string($conn, trim($_POST['mediatypeid']));
if (empty($mediatypeid)) 
{
echo "<p class='error'>Please enter a media type id</p>";
$valid = false;
}
if (!ctype_digit($mediatypeid)) 
{
echo "<p class='error'>Media Type ID must be integer</p>";
$valid = false;
}



//////////TEXT BOX///FORM LINE 4//////////////////// GENRE ID ////////
//////////PRESET ////////////////// TEXT /////
///SET to 1
$genreid = mysqli_real_escape_string($conn, trim($_POST['genreid']));
if (!ctype_digit($genreid)) {
echo "<p class='error'>Genre ID must be integer</p>";
$valid = false;
}



//////////TEXT BOX///FORM LINE 5/////////////////// COMPOSER ////////
//////////OPTIONAL ////////////////// TEXT ///// MAX 220 CHARS /////
$composer= mysqli_real_escape_string($conn, trim($_POST['composer']));
if (strlen($name>220))
{
echo "<p class='error'>(Name allows 220 characters max)</p>";
$valid=false;
}


//////////TEXT BOX///FORM LINE 6/////////////////// MILLISECONDS /////
//////////REQUIRED ////////////////// INTEGER ///////
$milliseconds = mysqli_real_escape_string($conn, trim($_POST['milliseconds']));
if (empty($milliseconds)) 
{
echo "<p class='error'>Please enter milliseconds</p>";
$valid = false;
}
if (!ctype_digit($milliseconds)) 
{
echo "<p class='error'>Milliseconds must be integer</p>";
$valid = false;
}



///////////TEXT BOX///FORM LINE 7/////////////////// BYTES/////////
////////// OPTIONAL ///////////////// INTEGER //////////
$bytes = mysqli_real_escape_string($conn, trim($_POST['bytes']));
if (!ctype_digit($bytes)) 
{
echo "<p class='error'>Bytes must be integer</p>";
$valid = false;
}


//////////TEXT BOX///FORM LINE 8////////////////// UNIT PRICE///////
//////////REQUIRED///////////// DECIMAL WITH 2 PLACES, MAX 99,999,999,99///////////////
$unitprice= mysqli_real_escape_string($conn, trim($_POST['unitprice']));
if(empty($unitprice) or !is_numeric($unitprice) or (!preg_match('/^[0-9]{1,10}(.[1-9]{2})?$/',$unitprice)))
{
echo"<p class='error'>numeric price as decimal required, 1-10 digits left of decimal and 2 right of decimal</p>";
$valid=false;
}

/////////////////////////////////////DONE GATHERING DATA FROM FORM////////////////////////////////////////


// if all the data collected is valid, update/OVERWRITE EXISTING DATA in the database w/query 
//and transfer HEADER DATA to the display page comp3partb2display.php
//and exit this page and go back to omp3partb2display.php!!!!!! HEADER will exit this file.
if ($valid) {
$query = "update Track set Name='$name', AlbumID='$albumid', MediaTypeId='$mediatypeid', GenreId='$genreid', Composer='$composer', Milliseconds='$milliseconds', Bytes='$bytes',UnitPrice='$unitprice' where TrackId=$tid";
mysqli_query($conn, $query) or die(mysqli_error($conn));
//if change was successful
if (mysqli_affected_rows($conn)>0) {
header("Location: comp3partb2display.php?actionheadervarupdatefromb4=updated&idheadervarupdatefromb4=$tid");
//SEND US TO partb2display where header info will trigger message confirming what we have done
exit();
}
else
{
//if NOTHING has changed
echo "<p class='error'>Unable to update record</p>";
}
}
}


else
{
//if CONFIRM BUTTON NOT CLICKED
//then no data sent from b4update confirm button, so START HERE

if (!isset($_GET['idLinkVarFromb2'])) 
//AKA#1 you used URL to get directly to this page
//ABNORMAL
{
echo "<p class='error'>No Data provided !!! Try starting from the <a href='comp3partb2display.php'>DISPLAY PAGE.</a>";
}


//AKA#2 you were just sent here from b2display update button...  you have not clicked CONFIRM update at the bottom of this form yet
//NORMAL


//pull database info into $tid, based on idLinkVar info sent from B2Display button
//info is used to populate the form fields on this page with the data originating from the row on which we clicked the update button back on B2Display

//THIS WILL RUN FIRST.... BEFORE the above PHP... BECAUSE the CONFIRM BUTTON WILL NOT HAVE BEEN CLICKED YET
//we need to run this first to populate the fields w/ the existing data from database

//but it CANNOT BE CODED FIRST because idLinkVar will be undefined......(not sure why 3-23-19-Sat-2007---resolved2041)
//....because after clicking CONFIRM on the form below...  we are RE RUNNING B4Update  
//in which case we are not coming from B2Display anymore..  
//so we ARE NOT sending idLinkVar from B2Display anymore on the second run.. therefore it will be undefined

//NOTE that the second run will NOT show THIS page(b4update) again... because it will redirect back to B2 Display before it gets to any of the FORM HTML for b4update

//FIRST RUN OF b4update upon clicking 'update' on B2Display, starts here

//SECOND RUN OF b4update upon clicking 'confirm' on B4UPDATE,THIS PAGE, starts at top

//INCLUDE this code INSIDE the brackets for if(!isset('idLinkVarFromb2')) becauase this code will not be needed on the SECOND RUN
//SECOND RUN= no data passed from idLinkVarFromb2

$tid=$_GET['idLinkVarFromb2'];
//ORIGIN OF TID in this file...  sent from display page based on which item we're updating
//the TID info is used to populate hidden tid form field
//php code above (line 31) draws from that form field 
$query = "Select * from Track where TrackId = $tid";
//use $tid here to pull info from database per our selection and fill these variables to populate the form fields
$result = mysqli_query($conn,$query);
if (!$result) 
{
die(mysqli_error($conn));
}
// check for results
if (mysqli_num_rows($result)> 0) 
{
// retrieve result row
$row = mysqli_fetch_assoc($result);
$name=$row['Name'];
$albumid=$row['AlbumId'];
$mediatypeid=$row['MediaTypeId'];
$genreid=$row['GenreId'];
$composer=$row['Composer'];
$milliseconds=$row['Milliseconds'];
$bytes=$row['Bytes'];
$unitprice=$row['UnitPrice'];

} 
else 
{
echo "<p class='error'>Unable to retrieve track $tid. <a href='comp3part2display.php'>Return to display page.</a>";
}

}




?>










<form method="post" action="comp3partb4update.php">






<!----/////////////////////FORM LINE 0
//////////////track id, sent from display page, used to pull the rest of the data into form fields before we udpate anything..  hidden field not visible on the form  ----->
<input type="hidden" name="tid" value="<?php echo $tid; ?>">


<!---//////////TEXT BOX///FORM LINE 1////////////////////// NAME aka Album Name //////////
//////////REQUIRED /////////////////// TEXT ///////// MAX 200 CHARS //////-->
<p>
<label for="name">Track Name:</label>
<input type="text" name="name" id="name"  value="<?php echo $name; ?>">
</p>


<!--//////////DROP DOWN SELECTION LIST///FORM LINE 2////////// ALBUM ID #////////
//////////REQUIRED ///////////////// DROP DOWN SELECTION /////////////////
Populates from database table////////-->	
<p>
<label for="albumid">Album ID:</label>
<select name="albumid" id="albumid">
<?php
$query = "Select AlbumId from Track"; //query TRACK TABLE
$result = mysqli_query($conn,$query);
if (!$result){
die(mysqli_error($conn));
}
if (mysqli_num_rows($result)> 0)
{ //if anything pulled from query..........
while ($row = mysqli_fetch_assoc($result)) 
{ //while something is there, aka fetch and print them ALL as dropdown items
echo "<option value='".$row['AlbumId']."'";

//$album represents existing number from 'albumid' field, gathered at line 21
// it is the existing NUMBER for MY database row and MY 'reportsto' since I clicked update for my name on 3.3
if ($albumid==$row['AlbumId']) 
{echo " selected ";
// if MY reportsto# matches someone's employee ID on the employee list as we work down the list, use that person
}
//that album ID will be the one who is SELECTED aka shown as preselected on the dropdown before it is clicked
//NOTE if we dont do this, the first albumID will be shown by default (that is how we did it in 3-5)
//NOTE all the others will be shown in the default order per database
echo ">".$row['AlbumId']."</option>";
//NOTE we are printing the albumID itself in our dropdown, not any other corresponding fields
}
}
?>
</select>
</p>


<!--//////////TEXT BOX///FORM LINE 3//////////////////// MEDIA TYPE ID /////
//////////PRESET ////////////////// TEXT /////
///SET to 2-->
<p>
<label for="mediatypeid">Media Type ID:</label>
<input type="text" name="mediatypeid" id="mediatypeid" value="<?php echo $mediatypeid; ?>" size="2">
</p>

<!--//////////TEXT BOX///FORM LINE 4//////////////////// GENRE ID ////////
//////////PRESET ////////////////// TEXT /////
///SET to 1//////-->	
<p>
<label for="genreid">Genre ID:</label>
<input type="text" name="genreid" id="genreid" value="<?php echo $genreid; ?>" size="2">
</p>

<!--//////////TEXT BOX///FORM LINE 5/////////////////// COMPOSER /////////
///////// OPTIONAL ///////////////// TEXT ///// MAX 220 CHARS /////-->	
<p>
<label for="composer">Composer:</label>
<input type="text" name="composer" id="composer" value="<?php echo $composer; ?>">
</p>

<!--//////////TEXT BOX///FORM LINE 6/////////////////// MILLISECONDS //////
////// REQUIRED ///////////////// INTEGER ///////-->	
<p>
<label for="milliseconds">Milliseconds:</label>
<input type="text" name="milliseconds" id="milliseconds" value="<?php echo $milliseconds; ?>">
</p>

<!--//////////TEXT BOX///FORM LINE 7/////////////////// BYTES/////////
////////// OPTIONAL ///////////////// INTEGER //////////-->	
<p>
<label for="bytes">Bytes:</label>
<input type="text" name="bytes" id="bytes" value="<?php echo $bytes; ?>">
</p>

<!--//////////TEXT BOX///FORM LINE 8////////////////// UNIT PRICE///////
////////REQUIRED///////////// DECIMAL WITH 2 PLACES, MAX 99,999,999,99///////////////-->
<p>
<label for="unitprice">Unit Price:</label>
<input type="text" name="unitprice" id="unitprice" value="<?php echo $unitprice; ?>">
</p>


<!--//////////BUTTON///FORM LINE 9/////////////////////-->
<p>
<input type="submit" name="confirmupdate" value=" Confirm Update">

</p>
</form>

<p>Cancel &  <a href="comp3partb2display.php">Return to Display Page</a></p>

<?php writeFoot("3B"); ?>