<?php

function priceCalc($priceArg,$quantityArg){
$discountArray=array(0,0,.05,.1,.2,.25);//no 1.5 per instructions
if ($quantityArg > 5){$quantityArg=5;}
$discountPrice=$priceArg-($priceArg*$discountArray[$quantityArg]);
$total=$discountPrice*$quantityArg;
return $total;
}


function writeHead($pageTitleArg1, $pageTitleArg2){
$headText=<<<EOD
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>$pageTitleArg1</title>
<meta name="viewport" content="width=device-width; initial-scale=1.0">
</head>
<body>


<div>
<header>
<h1 align="center">$pageTitleArg1</h1>
<h3 align="center">$pageTitleArg2</h2>
</header>
</div>

<nav align="center">
<a href="http://patti-bee2.dcccd.edu/banks/comp1main.php">Competency 1</a> |
<a href="http://patti-bee2.dcccd.edu/banks/comp2main.php">Competency 2</a> |
<a href="http://patti-bee2.dcccd.edu/banks/comp3main.php">Competency 3</a> |
<a href="http://patti-bee2.dcccd.edu/banks/comp4main.php">Competency 4</a>
</nav> 
<br>
EOD;
echo $headText;
}


function writeFoot(){
$footText=<<<EOD
<footer>
<p>
ITSE 1406 - Competency 3
<br>&copy Copyright by Michael Banks, 4-18-19
</p>
<a href="http://patti-bee2.dcccd.edu/banks/comp3main.php">Comp 3 Main</a>
</footer>

</div>

</body>

</html>
EOD;

echo $footText;
}

?>