<?php
require_once 'comp3functions.php';
$conn = createConn();

writeHead("Desired Competency 3-7", "Add Table");




$createOrderMB = "Create table OrderMB (OrderID INT(11) Primary Key auto_increment, CustomerId INT, OrderDate Date Not Null, OrderTotal Decimal(8,2) Not Null)";
if (mysqli_query($conn, $createOrderMB)) 
{echo 'success, Order table created';} 
else 
{echo "<p class='error'>Unable to create order table: ".mysqli_error($conn)."</p>";}


$createOrderLineMB = "Create table OrderLineMB (OrderLineID INT(11) Primary Key auto_increment, OrderId INT, TrackId INT, Quantity INT Not Null, UnitPrice Decimal(8,2) Not Null)";
if (mysqli_query($conn, $createOrderLineMB)) 
{echo 'success, orderline table created';} 
else 
{echo "<p class='error'>Unable to create orderline table: ".mysqli_error($conn)."</p>";}


/* */
$insertquery1 = "insert into ordermb values(default,'1234','2019-1-1','12.49')";
mysqli_query($conn, $insertquery1) or die(mysqli_error($conn));

$insertquery2 = "insert into orderlinemb values(default, 22, 22, 1	, 12.49)";
mysqli_query($conn, $insertquery2) or die(mysqli_error($conn));

$insertquery3 = "insert into orderlinemb values(default, 33, 33, 1	, 12.49)";
mysqli_query($conn, $insertquery3) or die(mysqli_error($conn));


$insertquery4 = "insert into ordermb values(default,'2234','2019-1-1','12.49')";
mysqli_query($conn, $insertquery4) or die(mysqli_error($conn));

$insertquery5 = "insert into orderlinemb values(default, 55, 55, 1	, 12.49)"; 
mysqli_query($conn, $insertquery5) or die(mysqli_error($conn));

$insertquery6 = "insert into orderlinemb values(default, 44, 44, 1	, 12.49)"; 
mysqli_query($conn, $insertquery6) or die(mysqli_error($conn));



// after inserting records, retrieve all records from new tables
$result = mysqli_query($conn, "Select * from ordermb");
// if records are retrieved, display on page.
if (mysqli_num_rows($result)>0) {
echo "<br><h3>orderMB table</h3><table><tr><th>OrderID</th><th>CustomerID</th><th>OrderDate</th><th>OrderTotal</th></tr>";
while ($row = mysqli_fetch_assoc($result)) {
echo "<tr><td>".$row['OrderID']. "</td>";
echo "<td>".$row['CustomerId']. "</td>";
echo "<td>".$row['OrderDate']. "</td>";
echo "<td>".$row['OrderTotal']. "</td>";
}
echo "</table>";
} 
else {
// if no records were retrieved from the table, display an error
echo "<p>No records to display</p>";
}


$result = mysqli_query($conn, "Select * from orderlinemb");
// if records are retrieved, display on page.
if (mysqli_num_rows($result)>0) {
echo "<br><h3>orderlineMB table</h3><table><tr><th>OrderLineID</th><th>OrderID</th><th>TrackID</th><th>Quantity</th><th>UnitPrice</th></tr>";
while ($row = mysqli_fetch_assoc($result)) {
echo "<tr><td>". $row['OrderLineID'] . "</td>";
echo "<td>". $row['OrderId'] . "</td>";
echo "<td>". $row['TrackId'] . "</td>";
echo "<td>". $row['Quantity'] . "</td>";
echo "<td>". $row['UnitPrice'] . "</td>";
}
echo "</table>";
} 
else {
// if no records were retrieved from the table, display an error
echo "<p>No records to display</p>";
}





writeFoot("3C");
?>




