

<?php
require_once 'comp3functions.php';
$conn = createConn();

writeHead("Desired Competency 3-7", "Add Table");
// Build the create table query
$query = "Create table BandMembers (MemberId INT(11) Primary Key auto_increment, LastName varchar(100) not null, FirstName varchar(100), BirthDate Date Not Null, DeathDate Date, ArtistId Int(11), YearJoined Int(4), YearLeft Int(4))";

// Execute create query. If it works, insert 2 records.
if (mysqli_query($conn, $query)) {
$query = "insert into BandMembers values(default,'McCartney','Paul','1942-06-18 00:00:00',null,8,1960,1970)";
mysqli_query($conn, $query) or die(mysqli_error($conn));
$query = "insert into BandMembers values(default,'Lennon','John','1940-10-09 00:00:00','1980-12-08 00:00:00',8,1960,1970)";
mysqli_query($conn, $query) or die(mysqli_error($conn));
// after inserting records, retrieve all records from new table
$result = mysqli_query($conn, "Select * from BandMembers");
// if records are retrieved, display on page.
if (mysqli_num_rows($result)>0) {
echo "<table><tr><th>ID</th><th>Name</th><th>Birth</th><th>Death</th><th>Artist Id</th><th>Joined</th><th>Left</th></tr>";
while ($row = mysqli_fetch_assoc($result)) {
echo "<tr><td>".$row['MemberId']."</td>";
echo "<td>".$row['FirstName']." ".$row['LastName']."</td>";
echo "<td>".$row['BirthDate']."</td>";
echo "<td>".$row['DeathDate']."</td>";
echo "<td>".$row['ArtistId']."</td>";
echo "<td>".$row['YearJoined']."</td>";
echo "<td>".$row['YearLeft']."</td>";
}
} else {
// if no records were retrieved from the table, display an error
echo "<p>No records to display</p>";
}

} else {
// if the table was not created successfully, write out an error
echo "<p class='error'>Unable to create BandMembers table: ".mysqli_error($conn)."</p>";
}

writeFoot(3.7);
?>
