

<?php
require_once 'comp3desired3-7functions.php';
writeHead("Desired Comp3.8", "Customer Display by Rep");
?>
<div><!--div ends on line 153-->


<?php
$conn = mysqli_connect("localhost", "phpstudent", "Itse1406", 'chinook');
// check to see if there is an offset in the querystring. If not set it to 0
if (isset($_GET['offset'])) 
{
$offset=$_GET['offset'];} 
else 
{$offset=0;}




//FIRST SELECTION OF A PERSON STARTS HERE

// calculate the offset for the previous link. If < 0, set to 0
$prev = $offset - 10;
if ($prev<0) {
$prev=0;
}


// calculate the offset for the next link
$next = $offset + 10;


// see how many records are in the table
$query = "select count(*) as quantityvar from Customer";//cnt contains number of records
$result=mysqli_query($conn,$query);
$row = mysqli_fetch_assoc($result);
$quantity=$row['quantityvar']; //$quantity now contains total number of records in customer table


// if the next offset is greater than the number of records,
// set it to 0 to loop back to the beginning of the table
if ($next > $quantity) {
$next = 0;
}
// check for user input
// initialize the rep to 0 in case no rep was selected.
/////////////////FIRST APPEARANCE OF $REP
$rep = 0;









// first check to see if the form DROPDOWN was submitted. 
//If so, get the rep from the form DROPDOWN SELECTION
//data will be sent via POST in this case
if 
(isset($_POST['rep'])) {$rep= $_POST['rep'];} 
elseif 
// if the form was not submitted, check the querystring to see if the rep was passed as part of the paging link
//paging link is PREVIOUS or NEXT.. should stay on the same rep in the dropdown
//data will be sent via GET in this case
(isset($_GET['rep'])) {$rep=$_GET['rep'];}
?>




<!-- add a form for the support rep -->
<!--FORM USES POST-->
<form method="post" action="comp3desired3-8script.php">
<p><label for="rep">Support Rep:</label>
<!--FORM LINE 1/////////////////DROP DOWN SELECTION/////////////////////////////----->
<select name="rep" id="rep">
<?php
// create a select option list from the employees table
$query = "select EmployeeId, LastName, FirstName from Employee";
$result = mysqli_query($conn,$query);
// check for errors
if (!$result) {
die(mysqli_error($conn));
}
if (mysqli_num_rows($result) > 0) {
while ($row = mysqli_fetch_assoc($result)) {
echo "<option value='".$row['EmployeeId']."'";
// if a rep has been entered, show that rep as the selected option
if ($rep==$row['EmployeeId']) {echo " selected";}

echo ">".$row['FirstName']." ".$row['LastName']."</option>";
}   
}
?>
</select>
<input type="submit" value="Select Support Rep">
</p>
</form>



<table>
<tr><th>ID</th><th>LastName</th><th>FirstName</th><th>Company</th><th>Phone</th><th>Fax</th><th>Email</th></tr>
<?php
// create the query. Use the offset as the starting row and limit the rows to 20.
$query = "Select CustomerId, LastName, FirstName, Company, Phone, Fax, Email from Customer";

// if a rep is found from the form or querystring, add the where clause for the selected rep
if ($rep>0) {
$query=$query." where SupportRepId =$rep";
}
// add the limit clause (must be after the where clause)
$query = $query." Limit $offset, 10"; //skip amt of offset and only produce 10 records max
//if you are not coming from paging link, but have clicked 'select support rep' instead...
//your offset is going to be 0

// run the query
$result = mysqli_query($conn,$query);
// check for errors
if (!$result) {
die(mysqli_error($conn));
}
// check for results
if (mysqli_num_rows($result)> 0) {
// loop through results and display
while ($row = mysqli_fetch_assoc($result)) {
echo "<tr><td>".$row['CustomerId']."</td>";
echo "<td>".$row['LastName']."</td>";
echo "<td>".$row['FirstName']."</td>";
echo "<td>".$row['Company']."</td>";
echo "<td>".$row['Phone']."</td>";
echo "<td>".$row['Fax']."</td>";
echo "<td>".$row['Email']."</td></tr>";
}













} //NOTE IF OFFSET EXCEEDS AMOUNT OF RECORDS FOR ANY GIVEN EMPLOYEE, THE TABLE OUTPUT WILL BE BLANK
//CLOSE BRCKET HERE SO THAT THE PRVS AND NEXT BUTTONS ARE NOT A CONDITION OF QUERY OUTPUT
//BECAUSE THE QUERY WILL PRODUCE NOTHING AFTER 2 OR 3 PAGES IF WE ARE SKIPPING AMOUNT OF OFFSET 10 PER PAGE BUT THE SALES REP ONLY HAS 20 OR 30 CLIENTS


// add the Previous and Next links in the last row of the table.
// include the offset and the rep in the querystring
// send data via GET... will show in url
echo 
"
<tr>
<td><a href='comp3desired3-8script.php?offset=$prev&rep=$rep'>PREVIOUS</a></td>

<td></td><td> </td><td> </td><td> </td><td> </td>

<td><a href='comp3desired3-8script.php?offset=$next&rep=$rep'>NEXT</a></td>
</tr>";

?>
</table>       
</div><!--end div from line 2-->
<?php writeFoot(); ?>
